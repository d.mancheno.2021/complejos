import math
from math import sqrt

class Complex(object):
    """Represents a complex number.

    Keyword arguments:
    real -- the real part (default 0.0)
    imag -- the imaginary part (default 0.0)
    """

    def __init__(self, real, imag=0.0):
        self.real = real
        self.imag = imag

    def __add__(self, other):
        """
        Add up two complex numbers. Returns a new complex with the sum. 
        This function doesn't change the complex numbers provided as parameters
        """
        return Complex(self.real + other.real, self.imag + other.imag)

    def __sub__(self, other):
        """
        Difference
        """
        return Complex(self.real - other.real, self.imag - other.imag)
    
    def __mul__(self, other):
        """
        Product
        """
        return Complex((self.real * other.real) - (self.imag * other.imag),
            (self.imag * other.real) + (self.real * other.imag))

    def __truediv__(self, other):
        """
        Quotient
        """
        r = (other.real**2 + other.imag**2)
        return Complex((self.real*other.real - self.imag*other.imag)/r,
            (self.imag*other.real + self.real*other.imag)/r)

    def __abs__(self):
        """
        Modulus
        """
        new = (self.real**2 + (self.imag**2)*-1)
        return Complex(sqrt(new.real))

    def __neg__(self):   # defines -c (c is Complex)
        return Complex(-self.real, -self.imag)

    #def __eq__(self, other):
    #    return self.real == other.real and self.imag == other.imag

    def __ne__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        return '({:.2f}{:+.2f}i)'.format(self.real, self.imag)

    def __repr__(self):
        return 'Complex' + str(self)

    def __pow__(self, power):
        raise NotImplementedError\
              ('self**power is not yet impl. for Complex')


c1 = Complex(2,-3)
print(c1)
c2 = Complex(0,1)
c2bis = Complex(0,1)
if (c2 == c2bis):
    print("Iguales")
else:
    print("Diferentes")  
print(c2)
print(c1+c2)