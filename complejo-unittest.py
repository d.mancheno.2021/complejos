

import unittest;

from Complejo import Complejos


class TestComplejos(unittest.TestCase):

    def test_sumar(self):

        c1 = Complejos(1,2)
        c2 = Complejos(3,4)

        suma = c1.sumar(c2)

        self.assertEqual(suma.real, 4)
        self.assertEqual(suma.imag, 6)

    def test_restar(self):

        c1 = Complejos(6,1)
        c2 = Complejos(8,4)

        restar = c1.restar(c2)

        self.assertEqual(restar.real, -2)
        self.assertEqual(restar.imag, -3)

    def test_multiplicar(self):
        pass

    def test_dividir(self):
        pass

if __name__ == "__main__":
    unittest.main()
