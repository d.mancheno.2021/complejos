class Complejos(object):
    def __init__(self, real, imag=0):
        self.real = real
        self.imag = imag

    def sumar(self, otro):
        result = Complejos(self.real + otro.real,
                            self.imag + otro.imag)
        return result

    def restar(self, otro):
        result = Complejos(self.real - otro.real,
                            self.imag - otro.imag)
        return result

    def multiplicar(self, otro):
        pass

    def dividir(self, otro):
        pass

    def igual(self, otro):
        return self.real == otro.real and self.imag == otro.imag 